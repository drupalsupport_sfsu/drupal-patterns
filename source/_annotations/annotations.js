var comments = {
    "comments": [
        {
            "el": "header[role=banner]",
            "title": "Masthead",
            "comment": "<p>The main header of the site doesn't take up too much screen real estate in order to keep the focus on the core content. It's using a linear CSS gradient instead of a background image to give greater design flexibility and reduce HTTP requests.</p>"
        },
        {
            "el": ".logo",
            "title": "Logo",
            "comment": "<p>The University offers this icon strictly for use on the Web, such as social media avatars. Please note that this icon should never be used in print materials such as letterhead.</p>"
        },
        {
            "el": "#nav",
            "title": "Navigation",
            "comment": "<p>Navigation for adaptive web experiences can be tricky. Top navigations are typical on desktop sites, but mobile screen sizes don't give us the luxury of space. We're dealing with this situation by creating a simple menu anchor that toggles the main navigation on small screens. This is just one method. <a href=\"http://bagcheck.com/\">Bagcheck</a> and <a href=\"http://contentsmagazine.com/\">Contents Magazine</a> add an anchor in the header that jumps users to the navigation which is placed in the footer. This solution works well because it doesn't require any Javascript in order to work. Other methods exist too. For example, <a href=\"http://m.espn.com\">ESPN's mobile navigation</a> overlays the main content of the page.</p><p>The nav is only hidden when a certain level of javascript is supported in order to ensure that users with little/poor javascript support can still access the navigation. Once the screen size is large enough to accommodate the nav, we show the main navigation links and hide the menu anchor.<p><p>See also: <a href=\"http://bradfrostweb.com/blog/web/responsive-nav-patterns/\">Responsive Navigation Patterns</a></p>"
        },
        {
            "el": ".search-form",
            "title": "Search",
            "comment": "<p>Search is an incredibly important priority, especially for mobile. It is a great idea to give users the ability to jump directly to what they are looking for without forcing them to wade through your site's navigation. Check out the <a href=\"http://burton.com\">Burton</a> and <a href=\"http://yelp.com\">Yelp</a> mobile sites for great examples of experiences that prioritize search.</p><p>We're also using the <a href=\"http://dev.w3.org/html5/markup/input.search.html\">HTML5 search input type</a>, which is great for mobile devices that can <a href=\"http://diveintohtml5.info/forms.html\">bring up the appropriate virtual keyboard</a> for many smartphones. And like the main header navigation, we're hiding the search form on small screens to save space. Clicking the search anchor toggles the form. </p>"
        },
        {
            "el": ".article-header h1",
            "title": "Article Header",
            "comment": "<p>The article header should be no more than 140 characters. </p>"
        },
        {
            "el": ".block-hero",
            "title": "Hero",
            "comment": "<p>The hero area highlights one major story using a large image and a captivating headline.</p>"
        },
        {
            "el": ".font-primary",
            "title": "Font Primary",
            "comment": "<p>The Lato font family are the University's primary typefaces and should be used whenever possible. </p>"
        },
        {
            "el": ".font-secondary",
            "title": "Font Secondary",
            "comment": "<p>If the primary typefaces are not available, the font OpenSans, common to both Windows and Macintosh operating systems, may be substituted.</p>"
        },
        {
            "el": ".sg-colors",
            "title": "Colors",
            "comment": "<p>The primary color palette is extended by using the secondary colors. These palettes were designed to allow individual departments to choose a variety of colors while maintaining a uniform palette across the SF State campus. The primary colors should be used whenever possible and should always be the dominant scheme to the secondary palette.</p><h3>Accessibility</h3> <p>Color contrast is needed....</p>"
        }
    ]

};
